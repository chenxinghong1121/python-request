from bs4 import BeautifulSoup
from queue import Queue
import requests, re, threading, pandas

url = 'https://www.pcone.com.tw/product/784#ref=d_category_category'

headers = {
    'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
}

response = requests.get(url, headers=headers)
bs = BeautifulSoup(response.text, 'html.parser')


# 商品名稱
def get_product_name():
    get_product_name = bs.find_all('div', class_='name limit-2-line', limit=190)
    product_name = [text.string for text in get_product_name]

    return product_name


# 商品價格
def get_product_price():
    get_product_price = bs.find_all('span', class_='symbol-price', limit=190)
    product_price = [text.string for text in get_product_price]

    return product_price


# 商品折扣
def get_product_discount():
    get_product_discount = bs.find_all('div', class_='best-price', limit=190)
    product_discount = [text.get('data-discount') for text in get_product_discount]

    return product_discount


# 商品網址
def get_seller_url():
    get_seller_url = bs.find_all('a', class_='product-list-item', limit=190)
    seller_url = ['https://www.pcone.com.tw' + text.get('href') for text in get_seller_url]

    return seller_url

# 店家資訊(名稱, 評價, 出貨天數, 回覆率)
def get_product_info(url):

    response = requests.get(url, headers=headers)
    bs = BeautifulSoup(response.text, 'html.parser')
        
    get_seller_name = bs.find('div', class_='store-name line-clamp medium-font')
    seller_name = get_seller_name.string

    get_product_rating = bs.find('div', class_='review pointer')
    product_rating_html = str(get_product_rating)
    match = re.search(r'\d+', product_rating_html)

    product_rating = match.group()

    get_product_day_and_reply = bs.find_all('p', class_='data medium-font')

    count = 0

    for text in get_product_day_and_reply:
        count += 1

        if count % 4 == 1 or count % 4 == 2:
            continue

        elif count % 4 == 3:
            product_day = text.string

        else:
            product_reply = text.string

    q.put((seller_name, product_day, product_reply, product_rating))

q = Queue()
threads = [threading.Thread(target=get_product_info, args=(product_url,)) for product_url in get_seller_url()]

for t in threads:
    t.start()

for t in threads:
    t.join()

results = [q.get() for _ in range(q.qsize())]

seller_names = []
product_days = []
product_replys = []
product_rating = []

for i in results:
    seller_names.append(i[0])
    product_days.append(i[1])
    product_replys.append(i[2])
    product_rating.append(i[3])

dict = {
    '商品名稱': get_product_name(),
    '商品價格': get_product_price(),
    '商品折扣': get_product_discount(),
    '店家名稱': seller_names,
    '商品評價': product_rating,
    '出貨天數': product_replys,
    '回覆率': product_replys
}

df = pandas.DataFrame(dict)

df.to_csv('output.csv')